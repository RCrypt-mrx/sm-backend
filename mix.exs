defmodule SmExbackend.MixProject do
  use Mix.Project

  def project do
    [
      app: :sm_exbackend,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {SmExbackend.Application, []}
    ]
  end

  defp deps do
    [
      {:corsica, "~> 1.2"},
      {:image, "~> 0.3.0"},
      {:poison, "~> 4.0"},
      {:plug, "~> 1.7"},
      {:cowboy, "~> 2.5"},
      {:plug_cowboy, "~> 2.0"}
    ]
  end
end
