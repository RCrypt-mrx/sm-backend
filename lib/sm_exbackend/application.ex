defmodule SmExbackend.Application do
  use Application

  def start(_type, _args) do
    Supervisor.start_link(children(), opts())
  end

  defp children do
    [ SmExbackend.Endpoint ]
  end

  defp opts do
    [
      strategy: :one_for_one,
      name: SmExbackend.Supervisor
    ]
  end
end
