defmodule SmExbackend.Router.Static do
  require Logger
  use Plug.Router

  plug Plug.Static, at: "/", from: :server

  plug(:match)
  plug(:dispatch)

  get "/" do
    response_path =
      case [Map.has_key?(conn.params, "type"), Map.has_key?(conn.params, "stamp")] do
        [true, true] ->
          Logger.info("GET [200] > /static/?type=#{conn.params["type"]}&stamp=#{conn.params["stamp"]}")
          "assets/stamps/#{conn.params["type"]}/#{conn.params["stamp"]}"
        _ ->
          Logger.info("GET [200] > /static/")
          "assets/static/no_params.png"
      end
    response_path = if File.exists?(response_path), do: response_path, else: "assets/static/not_found.png"
    conn
      |> put_resp_content_type("image/jpeg")
      |> send_file(200, response_path)
  end

  match _ do
    send_resp(conn, 404, "Page not found")
  end
end
