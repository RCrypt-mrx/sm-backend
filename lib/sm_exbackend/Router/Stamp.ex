defmodule SmExbackend.Router.Stamp do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  get "/" do
    response_body =
      case [Map.has_key?(conn.params, "type"), Map.has_key?(conn.params, "stamp")] do
        [true, true] -> SmExbackend.Controller.Search.by_type_stamp(conn.params["type"], conn.params["stamp"])
        [true, false] -> SmExbackend.Controller.Get.by_type(conn.params["type"])
        [false, true] -> SmExbackend.Controller.Search.by_stamp(conn.params["stamp"])
        [false, false] -> SmExbackend.Controller.Get.all()
      end
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(response_body))
  end

  post "/" do
    paramsJSON = conn.body_params
    response_body = SmExbackend.Controller.Change.run(paramsJSON["type"], paramsJSON["options"])
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(response_body))
  end

  put "/" do
    paramsJSON = conn.body_params
    response_body = SmExbackend.Controller.Change.run(
      paramsJSON["type"], paramsJSON["options"], paramsJSON["filename"])
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(response_body))
  end

  delete "/" do
    response_body =
      if Map.has_key?(conn.params, "stamp") do
        SmExbackend.Controller.Delete.run(conn.params["stamp"])
      else
        %{errors: 1, action: "Delete", msg: "Stamp filename parameter not defined"}
      end
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(response_body))
  end

  match _ do
    send_resp(conn, 404, "Page not found")
  end
end
