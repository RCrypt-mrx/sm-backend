defmodule SmExbackend.Controller.Search do
  def by_stamp(stamp_name) do
    scandirs =
      for dir <- File.ls!("assets/stamps") do
        dir_contents = File.ls!("assets/stamps/" <> dir)
        find_file = Enum.filter(dir_contents, fn element -> element == stamp_name end)
        unless length(find_file) == 0 do
          %{ found: true, stamp: hd(find_file), type: dir}
        else
          %{ found: false }
        end
      end
    scandir = Enum.filter(scandirs, fn element -> element.found end)
    if length(scandir) != 0, do: hd(scandir), else: %{found: false, msg: "No such file found"}
  end

  def by_type_stamp(type_name, stamp_name) do
    dirs = File.ls!("assets/stamps")
    dir_name = Enum.find(dirs, fn dir -> dir == type_name end)
    if dir_name do
      dir_contents = File.ls!("assets/stamps/" <> dir_name)
      file_search = Enum.find(dir_contents, fn file ->
        file == stamp_name
      end)
      if file_search do
        %{ found: true, type: dir_name, stamp: file_search }
      else
        %{ found: false, msg: "File not found"}
      end
    else
      %{ found: false, msg: "Dir not found"}
    end
  end
end
