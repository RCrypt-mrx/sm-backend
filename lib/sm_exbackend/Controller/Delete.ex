defmodule SmExbackend.Controller.Delete do
  def run(stamp_name) do
    try do
      stamp = SmExbackend.Controller.Search.by_stamp(stamp_name)
      if stamp.found do
        file_path = "assets/stamps/#{stamp.type}/#{stamp.stamp}"
        File.rm!(file_path)
        %{errors: 0, action: "Delete", type: stamp.type, stamp: stamp.stamp}
      else
        %{errors: 1, action: "Delete", msg: "Stamp doesn't exist"}
      end
    rescue
      e -> %{errors: 1, action: "Delete", stamp: stamp_name, msg: "Unexpected error: #{e.message}"}
    end
  end
end
