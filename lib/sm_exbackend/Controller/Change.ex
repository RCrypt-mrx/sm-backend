defmodule SmExbackend.Controller.Change do
  def run(stamp_type, options, filename \\ nil, debug \\ false) do
    operation = if filename, do: "Update", else: "Create"
    try do
      result =
        case stamp_type do
          "digital_sign" -> digital_sign_stamp(options, filename, debug)
          "entry_document" -> entry_document_stamp(options, filename, debug)
          "signer" -> signer_stamp(options, filename, debug)
          _ -> false
        end
      if result do
        %{errors: 0, action: operation, type: stamp_type, stamp: result}
      else
        %{errors: 1, action: operation, type: stamp_type, msg: "Stamp type not supported"}
      end
    rescue
      e -> %{errors: 1, action: operation, type: stamp_type, msg: "Unexpected error: #{inspect e}"}
    end
  end

  defp digital_sign_stamp([cert_number, cert_owner, cert_expire], filename, debug) do
    text_options = [
      font_size: 34,
      font: "Times New Roman",
      text_fill_color: "#4E3CD8"
    ]
    {:ok, default_frame} = Image.open("assets/frames/rounded_rectangle.png")
    {:ok, default_emblem} = Image.open("assets/emblems/double_eagle.png")
    {:ok, default_title1} = Image.Text.simple_text("ДОКУМЕНТ ПОДПИСАН", text_options)
    {:ok, default_title2} = Image.Text.simple_text("УСИЛЕННОЙ КВАЛИФИЦИРОВАННОЙ", text_options)
    {:ok, default_title3} = Image.Text.simple_text("ЭЛЕКТРОННОЙ ПОДПИСЬЮ", text_options)
    {:ok, certificate} = Image.Text.simple_text("Сертификат: #{cert_number}", text_options)
    {:ok, owner} = Image.Text.simple_text("Владелец: #{cert_owner}", text_options)
    {:ok, expiration} = Image.Text.simple_text("Действителен: #{cert_expire}", text_options)
    stamp =
      default_frame
      |> Image.compose!(Image.resize!(default_emblem, 180), [x: 90, y: 70])
      |> Image.compose!(default_title1, [x: 415, y: 105])
      |> Image.compose!(default_title2, [x: 285, y: 150])
      |> Image.compose!(default_title3, [x: 365, y: 195])
      |> Image.compose!(certificate, [x: 110, y: 270])
      |> Image.compose!(owner, [x: 110, y: 315])
      |> Image.compose!(expiration, [x: 110, y: 360])
    file_path = "assets/stamps/digital_sign"
    file_name = if filename, do: filename, else: "#{length(File.ls!(file_path))}ds.png"
    if debug, do: Image.write!(stamp, "assets/new.png"), else: Image.write!(stamp, "#{file_path}/#{file_name}")
    file_name
  end

  defp entry_document_stamp([company_name, document_num, regdate], filename, debug) do
    text_options = [
      font_size: 70,
      font: "Arial",
      text_fill_color: "#4E3CD8"
    ]
    {:ok, default_frame} = Image.open("assets/frames/rectangle55x20.png")
    {:ok, title} = Image.Text.simple_text(company_name, text_options)
    {:ok, number} = Image.Text.simple_text("Вход. № #{document_num}", text_options)
    {:ok, date} = Image.Text.simple_text(regdate, text_options)
    stamp =
      default_frame
      |> Image.compose!(title, [x: 55, y: 50])
      |> Image.compose!(number, [x: 55, y: 160])
      |> Image.compose!(date, [x: 55, y: 270])
    file_path = "assets/stamps/entry_document"
    file_name = if filename, do: filename, else: "#{length(File.ls!(file_path))}ed.png"
    if debug, do: Image.write!(stamp, "assets/new.png"), else: Image.write!(stamp, "#{file_path}/#{file_name}")
    file_name
  end

  defp signer_stamp([signer_ini, authority_num, regdate], filename, debug) do
    text_options = [
      font_size: 100,
      font: "Arial",
      text_fill_color: "#4E3CD8"
    ]
    {:ok, default_frame} = Image.open("assets/frames/rectangle57x21.png")
    {:ok, authority} = Image.Text.simple_text("По Доверенности №#{authority_num}", text_options)
    {:ok, signer} = Image.Text.simple_text("#{signer_ini}", text_options)
    {:ok, registration} = Image.Text.simple_text("от #{regdate}", text_options)
    stamp =
      default_frame
      |> Image.compose!(authority, [x: 55, y: 50])
      |> Image.compose!(signer, [x: 330, y: 170])
      |> Image.compose!(registration, [x: 315, y: 280])
    file_path = "assets/stamps/signer"
    file_name = if filename, do: filename, else: "#{length(File.ls!(file_path))}sa.png"
    if debug, do: Image.write!(stamp, "assets/new.png"), else: Image.write!(stamp, "#{file_path}/#{file_name}")
    file_name
  end
end
