defmodule SmExbackend.Controller.Get do
  def all do
    dirs = File.ls!("assets/stamps")
    for dir <- dirs do
      %{ type: dir, stamps: Enum.filter(File.ls!("assets/stamps/" <> dir), fn x -> x != "new.png" end) }
    end
  end

  def by_type(type_name) do
    dirs = File.ls!("assets/stamps")
    dir_name = Enum.find(dirs, fn dir -> dir == type_name end)
    if dir_name do
      %{found: true, type: dir_name, stamps: Enum.filter(File.ls!("assets/stamps/" <> dir_name), fn x -> x != "new.png" end) }
    else
      %{found: false, msg: "Dir not found"}
    end
  end
end
