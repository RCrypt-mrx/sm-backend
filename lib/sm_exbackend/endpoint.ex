defmodule SmExbackend.Endpoint do
  use Plug.Router

  plug SmExbackend.Policy
  plug(:match)
  plug(Plug.Parsers,
    parsers: [:urlencoded, :json],
    pass: ["application/json"],
    json_decoder: Poison
  )

  plug(:dispatch)

  forward("/static", to: SmExbackend.Router.Static)
  forward("/api", to: SmExbackend.Router.Api)

  match _ do
    send_resp(conn, 404, "Page not found")
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  def start_link(_opts) do
    Plug.Cowboy.http(__MODULE__, [])
  end
end
